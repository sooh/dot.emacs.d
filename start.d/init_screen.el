;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; frame title
(setq frame-title-format "(´・ω・`)")

;; mode line
(progn
  (setq display-time-24hr-format 1)     ; 24時間表示
  ;; (setq display-time-format "%m/%d(%a) %H:%M") ; 月日時分の表示順に %Y- ← 年表示
  ;; (setq display-time-day-and-date t)              ; 日時を表示
  (setq display-time-interval 30)                 ; インターバル
  (display-time))
(line-number-mode 1)
(column-number-mode 1)

;; inhibit scrollbars
(setq default-frame-alist
      (append (list '(menu-bar . -1)
                    '(vertical-scroll-bars . nil))
              default-frame-alist))

;; 行末で折返し
(set-default 'truncate-lines nil)
(setq truncate-partial-width-window 1)
(setq truncate-partial-width-windows nil) ; 縦分割時も有効に

;; 対応するカッコは光らせる
(show-paren-mode t)

;; linum-mode setting
(setq linum-format "%4d") ; 表示領域設定

;; ediff control panel は作成しない
(setq ediff-window-setup-function 'ediff-setup-windows-plain)


(if window-system
    (progn
      ;; fullscreen 関数ないので数値を指定してます
      (set-frame-height nil 56)
      (set-frame-width nil 99)
      ))

;; hl-line+
(require 'hl-line+)
(toggle-hl-line-when-idle)

;; generic
(require 'generic)

;; add functions
; ----------------------------------------
;; toggle-fullscreen
;; (defun toggle-fullscreen (&optional f)
;;   (interactive)
;;   (let ((current-value (frame-parameter nil 'fullscreen)))
;;     (set-frame-parameter nil 'fullscreen
;;                          (if (equal 'fullboth current-value)
;;                              (if (boundp 'old-fullscreen) old-fullscreen nil)
;;                            (progn (setq old-fullscreen current-value)
;;                                   'fullboth))))
;;   (redraw-display))     ; ときどき画面が崩れるので，再描画
                           ; サイズの大きなファイル開いてるとアレかもしんない

;; window-resizer
;; See `http://d.hatena.ne.jp/mooz/20100119/p1'
;; q でだけ抜けるようにいじってる
(defun mooz-window-resizer ()
  "Control window size and position."
  (interactive)
  (let ((window-obj (selected-window))
        (current-width (window-width))
        (current-height (window-height))
        (dx (if (= (nth 0 (window-edges)) 0) 1
              -1))
        (dy (if (= (nth 1 (window-edges)) 0) 1
              -1))
        c)
    (catch 'end-flag
      (while t
        (message "size[%dx%d]"
                 (window-width) (window-height))
        (setq c (read-char))
        (cond ((= c ?l)
               (enlarge-window-horizontally dx))
              ((= c ?h)
               (shrink-window-horizontally dx))
              ((= c ?j)
               (enlarge-window dy))
              ((= c ?k)
               (shrink-window dy))
              ;; otherwise
              ;; (t
              ((= c ?q)                 ; q で抜けるように
               (message "Quit")
               (throw 'end-flag t))
              )))))

;; keymapping
; ----------------------------------------
;; let で包んだほうが良いと思うんだけど，上手くできない
;; key binds for window split
(define-key ctl-q-map (kbd "1") 'delete-other-windows)                  ; C-x 1
(define-key ctl-q-map (kbd "2") 'split-window-vertically)               ; C-x 2
(define-key ctl-q-map (kbd "3") 'split-window-horizontally)             ; C-x 3
(define-key ctl-q-map (kbd "4") 'follow-delete-other-windows-and-split) ; follow-mode 
(define-key ctl-q-map (kbd "0") 'delete-window)                         ; C-x 0
;; window move
(define-key ctl-q-map (kbd "C-h") 'windmove-left)
(define-key ctl-q-map (kbd "C-j") 'windmove-down)
(define-key ctl-q-map (kbd "C-k") 'windmove-up)
(define-key ctl-q-map (kbd "C-l") 'windmove-right)
;; run toggle-fullscreen
;; (define-key ctl-q-map (kbd "f") 'toggle-fullscreen)
;; activate window-resizer
(define-key ctl-q-map (kbd "r") 'mooz-window-resizer)
;; run toggle-truncate-lines
(define-key ctl-q-map (kbd "l") 'toggle-truncate-lines)


;; 起動時に最大化
;; For CocoaEmacs needs experimental/hackers only patch
;; (when (eq window-system 'mac)
;;   (add-hook 'window-setup-hook
;;             (lambda ()
;;               (setq mac-autohide-menubar-on-maximize 0)
;;               (set-frame-parameter nil 'fullscreen 'fullboth)
;;               ;; (split-window-horizontally)
;;               )))

;; (defun mac-toggle-max-window ()
;;   (interacitve)
;;   (if (frame-parameter nil 'fullscreen)
;;       (set-frame-parameter nil 'fullscreeen nil)
;;     (set-frame-parameter nil 'fullscreen 'fullboth)))


(provide 'init_screen)
