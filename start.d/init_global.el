;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; バックアップファイルを残さない
(setq backup-inhibited 1)
(setq delete-auto-save-files 1)

;; auto-save-list の保存先指定
(setq auto-save-list-file-prefix "~/.emacs.d/var/auto-save-list/")

;; ベルを鳴らさない
(setq ring-bell-function 'ignore)

;; 1行ずつスクロール
(setq scroll-step 1)

;; 行移動時にカーソル位置を変更しない
(setq scroll-preserve-screen-position t)

;; スタートメニューを非表示
(setq inhibit-startup-message t)

;; ツールバーを非表示に
(if window-system
    (progn
      (tool-bar-mode -1)))


;; タイムロケールを英語に
(setq system-time-locale "C")

;; 終了時に開く
;; (setq confirm-kill-emacs 'y-or-n-p)

;; *scratch* の中身を空に
(setq initial-scratch-message nil)

;; ファイル名補完で大文字小文字を区別しない
(setq completion-ignore-case t)

;; ファイル末尾の自動改行はしない
(setq next-line-add-newlines nil)

;; システムの IM を無視
(setq mac-use-input-method-on-system nil)

;; TAB はスペース4個分を基本に
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

;; yes-or-no を y-or-n に
(fset 'yes-or-no-p 'y-or-n-p)

;; uniquify
(require 'uniquify)
;; filename<dir> 形式のバッファ名にする
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
;; * で囲まれたバッファは対象外にする
(setq uniquify-ignore-buffers-re "*[^*]+*")

;; cua-mode
(setq cua-enable-cua-keys nil)
(cua-mode t)


(provide 'init_global)
