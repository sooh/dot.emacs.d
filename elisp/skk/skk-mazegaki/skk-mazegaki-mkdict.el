;;; skk-mazegaki-mkdict.el --- Generating a mazegaki dictionary for SKK

;; Copyright (C) 2010  HAMANO Kiyoto

;; Author: HAMANO Kiyoto <khiker.mail+elisp@gmail.com>
;; Keywords: japanese, mule, input method, skk, dictionary

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; SKK 用の交ぜ書き辞書を生成する関数を提供します。

;;; Config:
;;
;; このファイルを `load-path' の通ったディレクトリに置
;; き、.emacs に以下を追加すれば設定は終わりです。
;;
;;   (require 'skk-mazegaki-mkdict)
;;
;; 実際に辞書を生成するには、以下のようにコマンドを打ちます。
;;
;;   M-x skk-mazegaki-mkdict-make-dictionary
;;
;; すると、デフォルトでは、/tmp 以下に TMP というファイルがで
;; きています。これが重複エントリの削除やソートがなされていな
;; い交ぜ書き辞書です。以下のようにコマンドを打ち、重複エント
;; リの削除とソートを行えば辞書生成は終了です。
;;
;;   $ skkdic-expr /tmp/TMP | skkdic-sort > SKK-JISYO.L.mazegaki

;;; ChangeLog
;;
;; * 0.0.1 (2010/12/24)
;;   * Initial version

;;; Code:

(require 'skk-mazegaki)


;; Variables:

(defvar skk-mazegaki-mkdict-default-dictionary "~/Dropbox/skk-dict/SKK-JISYO.L"
  "*交ぜ書き辞書を生成するために使用する元なる辞書ファイルの名前。
値は、辞書名までをフルパスで指定しなければならない。
デフォルト値は、/usr/share/skk/SKK-JISYO.L。")

(defvar skk-mazegaki-mkdict-default-output-dictionary "~/Dropbox/skk-dict/SKK-JISYO.L.mazegaki"
  "*出力される辞書ファイルの名前。
値は、辞書名までをフルパスで指定しなければならない。
デフォルト値は、/tmp/TMP。")

(defvar skk-mazegaki-mkdict-hash-size 1000
  "*1つの候補から交ぜ書き辞書用のエントリを生成した際、一時的
に生成した候補を保持するハッシュのサイズ
デフォルト値は、1000。")

(defvar skk-mazegaki-mkdict-prog-list nil
  "*skk-mazegaki-mkdict 内で、`skk-mazegaki-search-candidates' 関
数を呼び出す際に使用する第2引数の値。

デフォルトでは、`nil' が渡される。")


;; Functions:

(defun skk-mazegaki-mkdict-make-dictionary (&optional dict output)
  "交ぜ書き辞書を生成する関数。

生成される辞書はソートされていないので、skktools に含まれるコ
マンドを使って生成する必要がある。

例えば、この関数を実行した結果、/tmp/TMP に辞書ファイルができ
たならば、以下のように打つ事で辞書の重複エントリの削除とソー
トを行う事ができる。

 $ skkdic-expr /tmp/TMP | skkdic-sort > SKK-JISYO.L.mazegaki

交ぜ書き辞書を生成するために使用する元なる辞書は、デフォルト
では、`skk-mazegaki-mkdict-default-dictionary'に指定された値
を使用する。これを変更したい場合、引数 `dict' に使用したい辞
書をフルパスで指定する。`dict' に指定した値は、一時的に、
`skk-large-jisyo' 変数に束縛される。

出力される辞書のファイル名は、デフォルトでは、
`skk-mazegaki-mkdict-default-output-dictionary' に指定された
値となる。/tmp 以下へと出力される。これを変更したい場合、
`output' 引数にフルパスで出力したいファイルの名前を指定する。"
  (interactive)
  (let ((skk-large-jisyo (if dict dict skk-mazegaki-mkdict-default-dictionary))
        (hash (make-hash-table :size skk-mazegaki-mkdict-hash-size))
        (output-file
         (if output output skk-mazegaki-mkdict-default-output-dictionary))
        header candidates)
    (when (interactive-p)
      (setq output-file (read-file-name
                         (format "出力ファイル名 (デフォルト %s)> "
                                 skk-mazegaki-mkdict-default-output-dictionary)
                         nil
                         skk-mazegaki-mkdict-default-output-dictionary)))
    ;; 指定されて辞書のエントリを全て調べて、出力ファイルに書き出す
    (let ((buf (find-file-noselect output-file)))
      (with-temp-buffer
        (erase-buffer)
        (insert-file-contents skk-large-jisyo)
        (goto-char (point))
        (while (re-search-forward "^\\([^ ]*\\) /\\(.*\\)$" nil t)
          (setq header     (match-string 1)
                candidates (match-string 2))
          (when (and (> (length header) 1)
                     (string-match "^>?[あ-ん]*[a-zA-Z]?$" header)
                     candidates)
            (when (string-match "^\\([^a-zA-Z]*\\)[a-zA-Z]" header)
              (setq header (match-string 1 header)))
            (when (string-match "^>\\(.*\\)$" header)
              (setq header (match-string 1 header)))
            (when (setq hash (skk-mazegaki-mkdict-make-mazegaki-word header))
              ;; 実際にファイルに書き出す。
              (maphash '(lambda (x y)
                          (let ((coding-system-for-write 'euc-jp))
                            (with-current-buffer buf
                              (goto-char (point-max))
                              (message "%s: %S" x y)
                              (let ((line (format "%s /" x)))
                                (dolist (i y)
                                  (setq line (concat line (format "%s/" i))))
                                (insert line))
                              (insert "\n"))))
                       hash)))))
      ;; バッファを保存
      (when buf
        (let ((coding-system-for-write 'euc-jp))
          (with-current-buffer buf
            (save-buffer)))))))

(defun skk-mazegaki-mkdict-make-mazegaki-word (word)
  "単語 `word' から生成可能な交ぜ書き辞書用エントリを生成する
関数。結果は、キーを変換元となる文字列、バリューを変換候補の
リストを持つハッシュで返される。"
  (let* ((converts (skk-mazegaki-search-candidates
                    word skk-mazegaki-mkdict-prog-list))
         (indexes  (skk-mazegaki-mkdict-breakup-word word))
         (hash (make-hash-table :size skk-mazegaki-mkdict-hash-size
                                :test 'equal))
         (skk-auto-okuri-process nil)
         candidates n mgk-word)
    (when (> (length word) 1)
      (dolist (i converts)
        ;; EUC-JP で encode できる文字のみを対象とする
        (unless (unencodable-char-position 0 (length i) 'euc-jp nil i)
          (dolist (j indexes)
            (setq candidates (skk-mazegaki-search-candidates
                              j skk-mazegaki-mkdict-prog-list))
            (dolist (k candidates)
              (when (and
                     (not (unencodable-char-position
                           0 (length k) 'euc-jp nil k))
                     ;; 漢字、ひらがな、カタカナ混じりの文字列である
                     (string-match (format "^[ぁ-ゞァ-ヾ%c-%c]+$"
                                           ;; Unicode における漢字の範囲
                                           #x3400 #x2ffff)
                                   k)
                     ;; ひらがな・カタカナのみでない
                     (not (string-match "^[ぁ-ゞァ-ヾ]+$" k))
                     (string-match k i))
                (when (and (not (string= j word))
                           (setq n (string-match j word)))
                  (setq mgk-word (concat (substring word 0 n)
                                         k
                                         (substring word (+ n (length j)))))
                  (when (not (string= mgk-word i))
                    (cond
                     ((gethash mgk-word hash)
                      (puthash mgk-word
                               (nreverse (delete-dups
                                          (cons i (gethash mgk-word hash))))
                               hash))
                     (t
                      (puthash mgk-word (list i) hash))))))))))
      hash)))

(defun skk-mazegaki-mkdict-breakup-word (word)
  "文字列 `word' を交ぜ書き辞書生成用にリストに分解する関数。

例えば、「きしゃ」という文字列ならば、以下のように分解される。
(\"き\" \"きし\" \"きしゃ\" \"し\" \"しゃ\" \"ゃ\")"
  (let* ((sublist #'(lambda (lis s e)
                      (let ((c 0)
                            l)
                        (dolist (i lis)
                          (when (and (>= c s) (<= c e))
                            (setq l (cons i l)))
                          (setq c (1+ c)))
                        (when l
                          (nreverse l)))))
         (lis (string-to-list word))
         (len (length lis))
         result)
    (dotimes (i len)
      (dotimes (j (- len i))
        (let ((j (+ j i)))
          (setq result
                (cons (mapconcat 'char-to-string (funcall sublist lis i j) nil)
                      result)))))
    (nreverse result)))


(provide 'skk-mazegaki-mkdict)

;;; skk-mazegaki-mkdict.el ends here

