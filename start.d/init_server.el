;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-


;; see@ http://d.hatena.ne.jp/syohex/20101224/1293206906
(require 'server)
(unless (server-running-p)
  (server-start))

(provide 'init_server)
