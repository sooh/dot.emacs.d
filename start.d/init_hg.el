;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'mercurial)
(require 'mq)

(provide 'init_hg)
