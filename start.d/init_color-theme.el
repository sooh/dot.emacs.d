;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; color-theme settings
;; (setq color-theme-libraries "~/.emacs.d/misc/scheme/")
(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (require 'zenburn-theme)))
;; colors additional
(if window-system (progn
  (set-background-color "black")
  (set-frame-parameter nil 'alpha 80)
  ))


(provide 'init_color-theme)
