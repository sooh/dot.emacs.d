;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(setq auto-mode-alist
      (cons '("\\.py$ . python-mode") auto-mode-alist))
(autoload 'python-mode "python-mode" "python editing mode." t)


(provide 'init_langs)
