;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'popup-select-window)

;; add function
;; via http://d.hatena.ne.jp/rubikitch/20100210/emacs
(defun split-or-pp-slt-window ()
  (interactive)
  (when (one-window-p)
    (if (> (frame-width) 50)
        (split-window-horizontally)
      (split-window-vertically)))
  (popup-select-window))

;; activate popup-select-window
(cond (window-system
      (define-key global-map (kbd "C-q C-q") 'split-or-pp-slt-window)
      )
      (t
       (define-key global-map (kbd "C-t") 'split-or-pp-slt-window)))

(define-key popup-select-window-popup-keymap "q" 'popup-select-window-next)
(define-key popup-select-window-popup-keymap "w" 'popup-select-window-previous)
;; popup.el から，popup-select をひっぱってくる
(define-key popup-select-window-popup-keymap (kbd "C-q") 'popup-select)
;; popup-select-window 中にelscreen 移動
;; (しようと思ったけどwindows に移行したので放置中
;; (移動はできるけど，popupの再構成がうまくいかない
;; (define-key popup-select-window-popup-keymap (kbd "p") 'elscreen-previous)


(provide 'init_popup-select-window)
