;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'wdired)

;; keybinds
(add-hook 'dired-mode-hook
          '(lambda ()
             (local-set-key (kbd "O") 'moccur-grep)))
(add-hook 'dired-mode-hook
          '(lambda ()
             (local-set-key (kbd "o") 'anything-c-moccur-dired-do-moccur-by-moccur)))
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
(define-key dired-mode-map "j" 'dired-find-file-other-window)

(provide 'init_dired)
