;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(setq user-full-name "sooh")
(setq user-mail-address "sooh321@gmail.com")

;; debugg on
(setq debug-on-error t)

;; PATH
(if (fboundp 'normal-top-level-add-subdirs-to-load-path)
   (let* ((my-lisp-dir "~/.emacs.d")
	   (default-directory my-lisp-dir))
     (setq load-path (cons my-lisp-dir load-path))
     (normal-top-level-add-subdirs-to-load-path)))

(unless (boundp 'user-emacs-directory)
  (defvar user-emacs-directory (expand-file-name "~/.emacs.d")))
;; 引数を load-path へ追加 
(defun add-to-load-path (&rest paths)
  (let (path)
    (dolist (path paths paths)
      (let ((default-directory (expand-file-name (concat user-emacs-directory path))))
        (add-to-list 'load-path default-directory)
        (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
           (normal-top-level-add-subdirs-to-load-path))))))

;; 読み込みに失敗しても，そこでとまらないように
(defmacro eval-safe (&rest body)
  '(condition-case err
       (progn ,@body)
     (error (message "[eval-safe] %s" err))))
(defun load-safe (loadlib)
  (let ((load-status (load loadlib t)))
    (or load-status
        (message (format "[load-safe] failed %s" loadlib)))
    load-status))
(defun autoload-if-found (functions file &optional docstring interactive type)
  (if (not (listp functions))
      (setq functions (list functions)))
  (and (local-library file)
       (progn
         (dolist (function functions)
           (autoload function file docstring interactive type))
         t )))


;; Emacs のバージョン判別のための変数を定義
(defun x->bool (elt) (not (not elt)))
(defvar emacs22-p (equal emacs-major-version 22))
(defvar emacs23-p (equal emacs-major-version 23))
(defvar darwin-p (eq system-type 'darwin))
(defvar ns-p (featurep 'ns))
(defvar carbon-p (and (eq window-system 'mac) emacs22-p))
(defvar mac-p (and (eq window-system 'mac) emacs23-p))


;; loader
(require 'loader)

(provide 'init.el)
