;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'savekill)

;; 保存先の指定
(setq save-kill-file-name "~/.emacs.d/var/kill-ring-saved.el")


(provide 'init_savekill)
