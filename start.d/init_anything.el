;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'anything-config)
(require 'anything-startup)
(require 'anything-show-completion)

(setq anything-sources
      '(
        anything-c-source-ffap-line
        anything-c-source-ffap-guesser
        anything-c-source-buffers-list
        anything-c-source-file-name-history
        anything-c-source-files-in-current-dir+
        anything-c-source-extended-command-history
        anything-c-source-emacs-commands
        anything-c-source-kill-ring
        anything-c-source-bookmarks
        anything-c-source-buffer-not-found
        anything-c-source-emacs-functions
        ))

(setq anything-for-files-prefered-list
      '(
        anything-c-source-ffap-line
        anything-c-source-ffap-guesser
        anything-c-source-buffers-list
        anything-c-source-recentf
        anything-c-source-bookmarks
        anything-c-source-file-cache
        anything-c-source-files-in-current-dir+
        ))


;; anything-c-moccur
(require 'anything-c-moccur)
(setq anything-c-moccur-anything-idle-delay 0.2
      anything-c-moccur-higligt-info-line-flag t
      anything-c-moccur-enable-auto-look-flag t
      anything-c-moccur-enable-initial-pattern t)


;; descbinds-anything
(require 'descbinds-anything)
(descbinds-anything-install)
(setq descbinds-anything-window-style 'split-window-vertically)


;; keybinds

;; anything で c-; するとエスケープ
(define-key anything-map (kbd "C-;") 'abort-recursive-edit)
;; 移動
(define-key anything-map (kbd "C-p") 'anything-previous-line)
(define-key anything-map (kbd "C-n") 'anything-next-line)
(define-key anything-map (kbd "C-v") 'anything-next-source)
(define-key anything-map (kbd "M-v") 'anything-previous-source)
;; anything-find-files での, c-h の挙動を変更(誤爆するので).
(define-key anything-find-files-map (kbd "C-h") 'delete-backward-char)
(global-set-key (kbd "C-;") 'anything)
(global-set-key (kbd "C-x C-b") 'anything)
(global-set-key (kbd "M-x") 'anything-M-x)
(global-set-key (kbd "M-y") 'anything-show-kill-ring)
(define-key global-map (kbd "C-x C-c") 'describe-bindings)
;; anything-find-files
(define-key global-map (kbd "C-x C-f") 'anything-find-files)
;; anything-c-moccuer
(global-set-key (kbd "M-o") 'anything-c-moccur-occur-by-moccur)
(global-set-key (kbd "C-M-o") 'anything-c-moccur-dmoccur)
;; anything-kill-buffer
;; (define-key global-map (kbd "C-x k") 'anything-kill-buffers)


;; recentf
(setq recentf-max-saved-items 1000)
;; recentf に加えないファイルを正規表現で指定
(setq recentf-exclude '("/TAGS$" "/var/tmp/"))
;; recentf の保存先を指定
(setq recentf-save-file "~/.emacs.d/var/recentf")

(require 'recentf-ext)


(provide 'init_anything)
