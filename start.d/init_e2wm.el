;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'e2wm)

;; activate e2wm
(define-key ctl-q-map (kbd "s") 'e2wm:start-management)
(define-key ctl-q-map (kbd "e") 'e2wm:stop-management)


(provide 'init_e2wm)
