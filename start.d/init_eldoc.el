;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'eldoc)
(require 'eldoc-extension)

(setq eldoc-idle-delay 0.2)
(setq eldoc-echo-area-use-multiline-p t)
(setq eldoc-minor-mode-string "")

(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)

(provide 'init_eldoc)
