;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(set-face-attribute 'default nil
                         :family "ricty discord"
                         :height 140)
(set-fontset-font nil
                  'japanese-jisx0208
                  (font-spec :family "ricty discord"))

;; aquaskkでbacksrashを入力
(define-key global-map [165] nil)
(define-key global-map [67109029] nil)
(define-key global-map [134217893] nil)
(define-key global-map [201326757] nil)
(define-key function-key-map [165] [?\\])
(define-key function-key-map [67109029] [?\C-\\])
(define-key function-key-map [134217893] [?\M-\\])
(define-key function-key-map [201326757] [?\C-\M-\\])


(provide 'init_fonts)
