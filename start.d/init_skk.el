;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(setq skk-user-directory "~/Dropbox/skk-dict/ddskk")
(when (require 'skk-autoloads nil t)
  (global-set-key (kbd "C-x C-j") 'skk-mode)
  ;; (setq skk-byte-compile-init-file t) ; 使ってないので切ってる
  )
(require 'skk-study)

;; ddskk の読み込みを Emacs の起動時に行う
(setq skk-preload t)


(if (eq system-type 'darwin)
    (progn (setq skk-server-host "localhost" ;AquaSKK のサーバ機能を利用
                 skk-server-portnum 1178) ;ポートは標準
           (add-to-list 'skk-completion-prog-list
                        '(skk-comp-from-jisyo "~/Dropbox/skk-dict/skk-jisyo.utf8")))
  )

;; 変換履歴をundo情報にしない. undo-tree.el と一緒に使うと挙動不審になるので.
(setq skk-undo-kakutei-word-only t)

;; 10 分間放置すると個人辞書を自動で保存
(defvar skk-auto-save-jisyo-interval 600)
(defun skk-auto-save-jisyo ()
  (skk-save-jisyo))
(run-with-idle-timer skk-auto-save-jisyo-interval
                     skk-auto-save-jisyo-interval
                     'skk-auto-save-jisyo)

;; 個人辞書の文字コードを指定する
(setq skk-jisyo-code 'utf-8)

;; (add-hook 'skk-search-excluding-word-pattern-function
;;           #'(lambda (kakutei-word)
;;              (and skk-abbrev-mode
;;                   (save-match-data
;;                     (string-match "\\*$" skk-henkan-key)))))

;; 句読点を英語に
(setq-default skk-kutouten-type 'en)

;; 英数は半角に
(setq skk-number-style nil)

;; @ の変換で西暦を利用
(setq skk-date-ad t)

;; 動的候補表示
(setq skk-dcomp-activate t) ; 動的補完
(setq skk-dcomp-multiple-activate t) ; 動的補完の複数候補表示
(setq skk-dcomp-multiple-rows 10) ; 動的補完の候補表示数

;; 動作
(setq skk-egg-like-newline t) ; enterで改行しない
(setq skk-delete-impies-kakutei nil) ; ▼モードで1つ前の候補を表示
(setq skk-show-annotation t) ; annotation
(setq skk-annotation-show-wikipedia-url t) ; annotationでwikiを
(setq skk-use-look t) ; 英語補完
(setq skk-auto-insert-paren nil) ; 閉じカッコを自動的に
(setq skk-henkan-strict-okuri-precedence t) ; 送り仮名が厳密に正しい候補を優先して表示
(require 'skk-hint)
(add-hook 'skk-load-hook
          (lambda ()
            (require 'context-skk)))

;; 言語
(setq skk-japanese-message-and-error t)
(setq skk-show-japanese-menu t)

;; isearch
(add-hook 'isearch-mode-hook 'skk-isearch-mode-setup)
(add-hook 'isearch-mode-end-hook 'skk-isearch-mode-cleanup)
(setq skk-isearch-mode 'latin)

;; skk-mazegaki
(require 'skk-mazegaki)
(setq skk-rom-kana-rule-list
      (append skk-rom-kana-rule-list
              '(("fj" nil skk-mazegaki-henkan))))
(setq skk-rule-tree
      (skk-compile-rule-list
       skk-rom-kana-base-rule-list skk-rom-kana-rule-list))

;; (require 'skk-mazegaki-mkdict)
;; skk-mazegaki-mkdict がうまく動かないときは -Q オプションとかで emacs を
;; 起動するといいんじゃないかと思った

(add-to-list 'skk-large-jisyo "~/Dropbox/skk-dict/SKK-JISYO.L")
(add-to-list 'skk-large-jisyo "~/Dropbox/skk-dict/SKK-JISYO.L.mazegaki")


(provide 'init_skk)
