;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'magit)
(require 'magit-svn)
(require 'magit-topgit)
(require 'magit-stgit)


(provide 'init_magit)
