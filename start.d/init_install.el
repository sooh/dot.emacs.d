;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'auto-install)
(setq auto-install-directory "~/.emacs.d/elisp/install")
;; auto-install-from-emacswiki のパッケージネームを emacs 起動時に更新
;; (auto-install-update-emacswiki-package-name t)


(provide 'init_install)
