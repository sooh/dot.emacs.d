;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; yasnippet とのバッティングへの対処
(add-hook 'org-mode-hook
          (lambda ()
            ;; YASnippet (using the new org-cycle hooks)
            (setq ac-use-overriding-local-map t)
            (make-variable-frame-local 'yas/trigger-key)
            (setq yas/trigger-key [tab])
            (add-to-list 'org-tab-first-hook 'yas/org-very-safe-expand)
            (define-key yas/keymap [tab] 'yas/next-field)))

(require 'org-install)
(setq org-startup-truncated nil)
(setq org-return-follows-link t)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(setq org-directory "~/Dropbox/memo")
(setq org-agenda-files "~/Dropbox/memo/agenda.org")
(setq org-capture-templates
      '(("t" "Todo" entry
         (file+headline nil "Inbox")
         "** TODO %?\n  %i\n %a\n  %t")
        ("b" "Bug" entry
         (file+headline nil "Inbox")
         "** TODO %?  :bug:\n  %i\n  %a\n  %t")
        ("i" "Idea" entry
         (file+headline nil "New Ideas")
         "** %?\n  %i\n  %a\n  %t")
        )
      )
(global-set-key (kbd "C-c c") 'org-capture)


;; org-babel
(require 'ob-tangle)
(require 'ob-R)                  ;; require R and ess-mode
(require 'ob-ruby)               ;; require ruby, irb, ruby-mode, and inf-ruby
(require 'ob-python)             ;; require python, and python-mode


(provide 'init_org)
