;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; encoding
(set-language-environment 'Japanese)
(set-language-environment 'utf-8)
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8-unix)
(cond
 (mac-p
  (require 'ucs-normalize)
  (setq file-name-coding-system 'utf-8-hfs)
  (setq local-coding-system 'utf-8-hfs)))
(set-terminal-coding-system 'utf-8-unix)
(set-keyboard-coding-system 'sjis-mac)


(provide 'init_encode)
