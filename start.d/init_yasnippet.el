;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'zencoding-mode)
(require 'yasnippet)
(setq yas/root-directory "~/.emacs.d/misc/dict/snippets")
;; (yas/initialize)
(yas/load-directory yas/root-directory)

;; メニューは使用しない
(setq yas/use-menu nil)

(defun yas/org-very-safe-expand ()
  (let ((yas/fallback-behavior 'return-nil)) (yas/expand)))

(provide 'init_yasnippet)
