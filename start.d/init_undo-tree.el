;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'undo-tree)
(global-undo-tree-mode)
;; mode-line の Undo-tree の表示を非表示に
(setq undo-tree-mode-lighter "")

;; mode keymap
(define-key undo-tree-visualizer-map "h" 'undo-tree-visualize-switch-branch-left)
(define-key undo-tree-visualizer-map "j" 'next-line)
(define-key undo-tree-visualizer-map "k" 'previous-line)
(define-key undo-tree-visualizer-map "l" 'undo-tree-visualize-switch-branch-right)
(define-key undo-tree-visualizer-map (kbd "C-m") 'undo-tree-visualizer-quit)

(provide 'init_undo-tree)
