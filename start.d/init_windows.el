;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; windows settings
(setq win:switch-prefix (kbd "C-q C-w"))
(require 'windows)
(setq win:use-frame nil)
(win:startup-with-window)
(setq win:configuration-file "~/.emacs.d/var/windows")

(define-key ctl-x-map "C" 'see-you-again)
(define-key ctl-q-map (kbd "C-n") 'win-next-window)
(define-key ctl-q-map (kbd "C-p") 'win-prev-window)
(define-key ctl-q-map (kbd "k") 'win-delete-current-window)

(provide 'init_windows)
