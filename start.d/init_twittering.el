;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; setting
(setq twittering-auth-method 'xauth)
(require 'twittering-mode)
(setq twittering-username "sooh__")
(setq twittering-status-format "%i %s %t %@")
(setq twittering-icon-mode nil)
(setq twittering-convert-fix-size 48)
(setq twittering-proxy-use nil)

;; face
(defun twittering-mode-hook-func ()
  (set-face-bold-p 'twittering-username-face t)
  (set-face-foreground 'twittering-username-face "DeepSkyBlue3")
  (set-face-foreground 'twittering-uri-face "Gray35")
  (follow-mode t)
  (setq truncate-line t)
  (setq truncate-partial-width-window nil)
  (setq truncate-partial-width-windows t)
)

(add-hook 'twittering-mode-hook
           'twittering-mode-hook-func
)

(add-hook 'twittering-new-tweets-hook
          (lambda ()
            'twittering-goto-first-status
            )
)

;; keymap
(define-key twittering-mode-map "i" nil)
(define-key twittering-mode-map (kbd "C-c RET") 'twittering-native-retweet)
(define-key twittering-mode-map "/" 'anything-c-moccur-occur-by-moccur)
(define-key twittering-mode-map "?" 'describe-bindings)
;; (define-key twittering-edit-mode-map "\C-p" "\C-c\C-c")
;; (define-key twittering-edit-mode-map "\C-k" "\C-c\C-k")


;; growl cf.http://d.hatena.ne.jp/meech/20101230/1293735436?utm_source=twitterfeed&utm_medium=twitter
;; (defvar growl-notify-path "/usr/local/bin/growlnotify")
;; (defvar twmode-icon-path "~/Downloads/twitter-icon.png")
;; (defun my-twmode-growl-notify ()
;;   (when (equal twittering-new-tweets-spec
;;                '(replies))
;;   (mapc (lambda (status)
;;           (my-twmode-growl-notify-1 (cdr (assq 'user-screen-name status))
;;                                     (cdr (assq 'text status))
;;                                     twittering-new-tweets-spec))
;;         twittering-new-tweets-statuses)))
;; (defun my-twmode-growl-notify-1 (user text spec)
;;   (cond
;;    ((and (equal spec '(replies))
;;          (not (string= user twittering-username)))
;;     (with-temp-buffer
;;       (insert "@" user "\n"
;;               text)
;;       (call-process-region (point-min)
;;                            (point-max)
;;                            growl-notify-path
;;                            nil nil nil
;;                            "-n" "twmode"
;; ;                           "--image" (expand-file-name twmode-icon-path)
;;                            )))))
;; (add-hook 'twittering-new-tweets-hook
;;           'my-twmode-growl-notify)


(provide 'init_twittering)
