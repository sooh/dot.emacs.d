;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'pos-tip nil t)
(require 'auto-complete-config)

(add-to-list 'ac-dictionary-directories "~/.emacs.d/misc/dict/auto-complete")
;;  補完候補のソート
(setq ac-use-comphist t)

;; ソートファイルの保存先を設定
(setq ac-comphist-file
      (expand-file-name (concat user-emacs-directory
                                 "/var/ac-comhist.dat")))
;; デフォルトの設定を有効にする
(ac-config-default)

;; 対象の全てで補完を有効にする
(global-auto-complete-mode t)

(setq default-enable-multibyte-characters t)
(setq ac-auto-show-menu 30)
(setq ac-ignore-case 'smart)
(setq ac-complete-file-name 1)
(setq ac-use-fuzzy 1)

;; (set-face-background 'ac-candidate-face "Gray")
(set-face-background 'ac-selection-face "Blue")
;; (set-face-underline 'ac-candidate-face "White")

(define-key ac-completing-map (kbd "C-m") 'ac-complete)
(define-key ac-completing-map (kbd "C-n") 'ac-next)
(define-key ac-completing-map (kbd "C-p") 'ac-previous)
(define-key ac-completing-map (kbd "M-/") 'ac-stop)


(provide 'init_auto-complete)
