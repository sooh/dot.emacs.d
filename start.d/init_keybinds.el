;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; Mac での装飾キー周りの設定
(when (eq system-type 'darwin)
  (setq mac-command-key-is-meta nil)     ; cmd を meta にしない
  (setq mac-option-modifier 'meta)       ; alt を meta に割り当て
  (setq mac-function-modifier 'meta)     ; fn を meta に割り当て
  (setq mac-command-modifier 'super)     ; cmd を super に割り当て
  (setq mac-pass-control-to-system nil)) ; ctrl はシステムに渡さない

;; C-h でバックスペース
(global-set-key (kbd "C-h") 'backward-delete-char)

;; C-j を無効に
(global-unset-key (kbd "C-j"))

;; C-m で改行
(global-set-key (kbd "C-m") 'newline-and-indent)

;; C-x C-k で kill-buffer
(global-set-key (kbd "C-x C-k") 'kill-buffer)

;; set "C-q" as prefix
(global-unset-key (kbd "C-q"))
(define-prefix-command 'ctl-q-map)
(define-key global-map (kbd "C-q") 'ctl-q-map)
(defvar ctl-q-map (make-sparse-keymap))

;; for terminal.app
(global-set-key (kbd "C-x C-b") 'anything)
(global-set-key (kbd "C-x b") 'list-buffers)

;; c-w c-w で削除, c-w w でコピー
;; (define-key global-map (kbd "C-w C-w") ')
;; (define-key global-map (kbd "C-w w") ')

;; M-x Exit で Emacs を終了
(defalias 'exit 'save-buffers-kill-emacs)


(provide 'init_keybinds)
