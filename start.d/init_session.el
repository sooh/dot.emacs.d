;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

;; via http://bitbuket.org/sakito/dot.emacs.d
;; 参考どころかパクらせてもらいました

(when (require 'session nil t)
  ;; 前回のカーソル位置, 開いたファイル履歴等を記録
  (setq session-initialize '(de-saveplace session keys menus places)
        session-globals-include '((kill-ring 500)
                                  (session-file-alist 500 t)
                                  (file-name-history 10000))
        ;; 保存時ではなく閉じた時のカーソル位置を記憶する
        session-undo-check -1)
  ;; 記憶容量を倍に設定
  (setq session-globals-max-size 2048)
  (setq session-globals-max-string 2048)
  (setq session-registers-max-string 2048)
  ;; ミニバッファ履歴リストの長さ制限を無くす
  (setq history-length t)
  ;; session で無視するファイルの設定
  (setq session-set-file-name-exclude-regrexp
        "/\\.overview\\|\\.session\\|News/\\||^/var/folders/\\|^/tmp/\\|\\.orig\\|\\.elc\\|\\.pyc\\|\\.recentf\\|\\.cache")
  ;; session 保存ファイルの場所
  (setq session-save-file
        (expand-file-name (concat user-emacs-directory
                                  "/var/session.cache")))
  ;; 30分で自動保存
  (defvar my-timer-for-session-save-session (run-at-time t (* 30 60) 'session-save-session))
  (add-hook 'after-init-hook 'session-initialize))

;; minibuffer history から重複を排除する
(defun minibuffer-delete-duplicate ()
  (let (list)
    (dolist (elt (symbol-value minibuffer-history-variable))
      (unless (member elt list)
        (push elt list)))
    (set minibuffer-history-variable (nreverse list))))
(add-hook 'minibuffer-setup-hook 'minibuffer-delete-duplicate)

;; kill-riing 内の重複を排除する
(defadvice kill-new (before ys:no-kill-new-dupliccates activate)
  (setq kill-ring (delete (ad-get-arg 0) kill-ring)))


(provide 'init_session)
