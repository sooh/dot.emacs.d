;; -*- mode: emacs-lisp; coding: utf-8-unix; -*-

(require 'color-moccur)
(require 'moccur-edit)

;; スペースで区切ってAND検索っぽく
(setq moccur-split-word t)

(provide 'init_moccur)
